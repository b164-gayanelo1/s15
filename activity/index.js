console.log("Hello, World!");

function activity() {
	
	// ACTIVITY PART 1
	let num1 = parseInt(prompt("Provide a number"));
	let num2 = parseInt(prompt("Provide another number"));

	function dynamiCalcu (num1,num2) {

		let checkSum = num1 + num2;
		let sum = num1 + num2;
		let diff = num1 - num2;
		let product = num1 * num2;
		let quotient = num1 / num2;
		
		if(checkSum < 10) {
			console.warn(`The sum of the two numbers is ${sum}`);
		}
		else if(checkSum >= 10 && checkSum <= 20) {
			alert(`The difference of the two numbers is ${diff}`);
		}
		else if(checkSum >= 21 && checkSum <= 29) {
			alert(`The product of the two numbers is ${product}`);
		}
		else if(checkSum >=30) {
			alert(`The quotient of the two numbers is ${quotient}`);
		}

	}
	
	dynamiCalcu(num1, num2);



	// ACTIVITY PART 2
	let name = prompt("What is your name?");
	let age = prompt("What is your age?");

	function nameGame(name,age) {

		if(!name || !age) {
			alert("Are you a time traveler?");
		}
		else if(name === name && age === age) {
			alert(`Hello ${name}. Your age is ${age}.`);
		}

	};
	
	nameGame(name,age);



	// ACTIVITY PART 3
	function isLegalAge(age) {
		
		if(age >= 18) {
			alert("You are of legel age.")
		}
		else if(age <= 17) {
			alert("You are not allowed here.")	
		}
	}

	isLegalAge(age);



	// ACTIVITY PART 4
	function ageChecker(age) {
		switch(age) {
			case "18":
				alert("You are now allowed to party.");
				break;
			case "21":
				alert("You are now part of the adult society");
				break;
			case "65":
				alert("We thank you for your contribution to the society");
				break;
			default:
				alert("Are you sure you're not an alien?")
				break;
		}
	}

	ageChecker(age);


	
	// Try,Catch,Finally !!!PLEASE UNCOMMENT ME <------------<<<
	// function isLegalAge_try(age) {
		
	// 	try {
	// 		console.warn(isLegalAge(age));
	// 		law // this is an error sample
	// 	}
	// 	catch (error){
	// 		console.log(typeof error);
	// 		console.warn(error.message)
	// 	}
	// 	finally {
	// 		console.log("This is a unit test.")
	// 	}
	// };

	// isLegalAge_try(age);

	alert("NOTICE:\nYou are not entering THE STRETCH GOALS section.\n\nEnjoy!")

	// STRETCH GOALS #1
	let day = prompt('Please enter a day: ').toLowerCase();

	function colorOfTheDay(day){
		switch(day){
			case "monday":
				alert(`Today is Monday. Wear Black.`);
				break;
			case "tuesday":
				alert(`Today is Tuesday. Wear Green.`);
				break;
			case "wednesday":
				alert(`Today is Wednesday. Wear Yellow.`);
				break;
			case "thursday":
				alert(`Today is Thursday. Wear Red.`);
				break;
			case "friday":
				alert(`Today is Friday. Wear Violet.`);
				break;
			case "saturday":
				alert(`Today is Saturday. Wear Blue.`);
				break;
			case "sunday":
				alert(`Today is Sunday. Wear White.`);
				break;
			default:
				alert(`Invalid Input. Enter a valid day of the week.`)
		}
	};

	colorOfTheDay(day);

	// STRETCH GOALS #2
	let checker = parseInt(prompt("Let's check your number: "));
	
	function oddEvenChecker(checker){

		if(typeof checker === 'number') {

			if(checker % 2 == 0) {
				alert("The number is even.")
			}
			else {
				alert("The number is odd.")
			}

		}

		else {
			alert("Invalid input.")
		}

	};

	oddEvenChecker(checker);




	// STRETCH GOALS #3
	let amount = parseInt(prompt('Please enter a spending amount: '));
	let budget = 40000;

	function budgetChecker(amount){

			if(typeof amount === "number" && amount >= budget){
				alert("You are over the buget.");
			}
			else if(typeof amount === "number" && amount <= budget){
				alert("You have resources left.")
			}
			else{
				alert("Invalid input")
			}

	};

	budgetChecker(amount);



};

activity();







