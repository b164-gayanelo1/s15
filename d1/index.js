console.log("Hello, World!");

// Assignment Operator
	let assignmentNumber = 8;
	console.log(assignmentNumber);

// Artihmetic Operators
	// + - * / %(modulo)


// Addition Assignment Operator(+=)
	assignmentNumber = assignmentNumber+2;
	console.log(assignmentNumber); //10

	// shorthand
	assignmentNumber += 2;
	console.log(assignmentNumber); //12

	// Subtraction/Multiplication/Division assignment operator (-=,*=,/=)
	assignmentNumber -=2;
	console.log(assignmentNumber);
	assignmentNumber *=2;
	console.log(assignmentNumber);
	assignmentNumber /=2;
	console.log(assignmentNumber);



// Multiple Operators and Parenthesis
/*
	- When multiple operators are applied in a single statement, it follows the PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction)

	- The operations were done in the ff order:
	1) 3 * 4 = 12
	2) 12 /5 = 2.4
	3) 1 + 2 = 3
	4) 3 - 2 = 1

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);



//Increment and Decrement Operator
	// Operators that add or subtract values by 1 and reassings the vlaue of the variable where the increment/decrement was applied to.

	let z = 1;

	// Increment
		// Pre-fix - The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment".

		++z
		console.log(z); // 2-The value of Z was added with 1 and is immediately returned.
		
		// Post fix - The value "z" is returned and stored in the variable "increment" then the value of "z" is increased by one.

		z++
		console.log(z); // 3 - The value of Z was added with 1.
		console.log(z++); // 3 - The previous value of the variable is returned.
		console.log(z); // 4 - A new value is now returned.

		// Pre-fis vs Post-fix incrementation
		console.log(z++); // 4
		console.log(z); // 5
		console.log(++z); // 6 - The new vlaue is returned immediately

		// Pre-fis vs Post-fix incrementation
		console.log(--z); // 5 - With pre-fix decrementation the result of subtraction by 1 is returned immediately.

		console.log(z--); // 5 - With post-fix decrementation the result of subtraction by 1 is not immediately returned, instead the previous value is returned first.
		console.log(z); // 4




// Type Coercion
	// Is the automatic or implicit conversion of values from one data type to another.
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion) //1012
console.log(typeof coercion);


// Adding/Concantenating a string and a number will result to a string.
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// The boolean "True" is also associated with the value of 1.
let numE = true + 1; 
console.log(numE); // =2 <-- number
console.log(typeof numE); // =2 <-- number

//The boolean "False" is also associated with the value of 0.
let numF = false + 1; 
console.log(numF);
console.log(typeof numF); // =1 <-- number



// Comparison Operators
let juan = 'juan';

	// (==) Equality Operator
		// Checks whether the operands are equal/have the same content
		// Attempts to CONVERT and COMPARE operands of different data types
		// This operator returns a boolean value.

		console.log(1 == 1); // true
		console.log(1 == 2); // false
		console.log(1 == '1'); // true
		console.log(0 == false); // true
		console.log('Juan' == 'JUAN'); // false, case sensitive
		console.log('juan' == juan); // true


	// (!=) Inequality Operator
		// Checks wither the operands are not equal/have different content
		console.log(1 != 1); //false
		console.log(1 != 2); //true
		console.log(1 != '1'); //false
		console.log(0 != false); //false
		console.log('juan' != 'JUAN'); //true
		console.log('juan' != juan); //false


	// (===) Strictly Equality Operator

		console.log(1 === 1); // true
		console.log(1 === 2); // false
		console.log(1 === '1'); // false >  different data types
		console.log(0 === false); // false > different data types
		console.log('Juan' === 'JUAN'); // false, case sensitive
		console.log('juan' === juan); // true


	// (!==) Strictly Equality Operator
		console.log('Strictly Equality Operator');
		console.log(1 !== 1); //false
		console.log(1 !== 2); //true
		console.log(1 !== '1'); //true
		console.log(0 !== false); //true
		console.log('juan' !== 'JUAN'); //true
		console.log('juan' !== juan); //false



// Relational Comparison Operators
	// Check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString= "5500";

	// (>) Greaer than
	console.log('Greater Than')
	console.log(x > y); // false
	console.log(w > y); // true

	// (<) Less than
	console.log('Less Than')
	console.log(w < x); // false
	console.log(y < y); // false
	console.log(x < 1000); // true
	console.log(numString < 1000); // true - forced coercion - always looks at the data type, it converts it
	console.log(numString < 6000); // true - forced coercion to change string to number
	console.log(numString < "Jose"); // true string to string

	// Greater Than or Equal to
	console.log('Greater than or equal to') // true
	console.log(w >= 8000) // true

	// Less Than or Equal to
	console.log('Less than or equal to') // true
	console.log(w <= y) // true
	console.log(y <= y) // true





// Logical Operator

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

	// Logical AND operator(&& - Double Ampersand)
		// Returns true if ALL operands are true.
		console.log('Logical AND Operator')


	let authorization1 = isAdmin && isRegistered
	console.log(authorization1); // false (since isAdmin = false)

	let authorization2 = isLegalAge && isRegistered
	console.log(authorization2); // true (since both are true)
	

	let requiredLevel = 95;
	let requiredAge = 18;

		let authorization3 = isRegistered && requiredLevel === 25;
		console.log(authorization3);

		let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
		console.log(authorization4);

	let userName = 'gamer2022';
	let userName2 = 'shadow1991';
	let userAge = 15;
	let userAge2 = 30;

		let registration1 = userName.length > 8 && userAge >= requiredAge;
			// .length is property of strings which determines the number of characters in the string.
		console.log(registration1);

		let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
		console.log(registration2)



		// Logical OR operator(|| - Double Pipe)
			// Returns true if atleast ONE of the operands are true.
		console.log('Logical AND Operator')

		let userLevel = 100;
		let userLevel2 = 65;

			let guildRequirement1 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
			console.log(guildRequirement1); // true


			// let guildRequirement1 = isRegistered && userLevel2 >= requiredLevel && userAge2 >= requiredAge;
			// console.log(guildRequirement1); // false - Since not all values are true

		let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
		console.log(guildRequirement2); // false 



		// NOT operator(!)
			// Turns a boolean into the opposite value.
		console.log('Logical AND Operator')

		let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
		console.log(guildAdmin); // true 


		console.log(!isRegistered); // false
		console.log(!isLegalAge); // false

		let opposite = !isAdmin;
		let opposite2 = !isLegalAge;

		console.log(opposite); // true - isAdmin original value = FALSE
		console.log(opposite2); // false - isLegalAge original value = TRUE


// if, else if, and else statement
	
// IF Statement
	// If statement will run a code block if the condition specified is true or results to true

	// if(true){
	// 	alert('We just run an if condition');
	// }

	let numG = -1;

	if(numG < 0){
		console.log('Hello');
	}

	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	if(userName3.length > 10) {
		console.log("Welcome to Game Online")
	};

	if(userLevel3 >= requiredLevel){
		console.log("You are qualified to join the guild!");
	}

	if(userName3.length >= 10 && isRegistered && isAdmin) {
		console.log("Thank you for joining the Admin!");
	} else {
		console.log("You are not ready to be an Admin");
	}


	// ELSE statement
		// The "else" statement executes a block of codes if all other conditions are false.

	if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
		console.log("Thank you for joining the Noobies Guild!");
	} else {
		console.log("You are too strong to be a noob. :(");
	}

	// ELSE IF statement
		// Executes the statement if the previous conditions are false.

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge) {
		console.log("Thanky you for the joining the Noobies Guild!");
	} else if(userLevel > 25) {
		console.log("You're too strong to be a noob");
	} else if(userAge3 < requiredAge) {
		console.log("You're too young to join the guild.")
	} else {
		console.log("Better luck next time.");
	}



// IF, ELSE IF and ELSE statement with functions

function addNum(num1, num2) {
	// check if the numbers being passed are number types.
	// "typeof" keyword returns a string which tells the type of data that follows it.

	if(typeof num1 === "number" && typeof num2 === "number" ) {
		console.log("Run only if both arguments passed ad number types");
		console.log(num1 + num2);
	} else {
		console.log("One or both of the arguments are not numbers");
	}

};

addNum(5, 1);

let helloWorld = true;
console.log(typeof helloWorld);


// Create Log In Function
function login(username, password) {
	if (typeof username === "string" && typeof password === "string"){
		console.log("Both arguments are strings.");
	} 
	 
	if(username.length >= 8 && password.length >= 8) {
		console.log("Thank you for loggin in!"); 
	} 

	else if (username.length < 8 && password.length < 8) {
		console.log("Credentials are too short!");
	} 

	else if (username.length <= 8){
		console.log("Username is too short!")
	} 

	else if(password.length <= 8){
		console.log("Password is too short!")
	}
	 
	else {
		console.log("One of the arguments is not a string");
	}
}
login("asasdfdf", "asdfasdf");

		/*
			Nested if-else
				will run if the parent if statement is able to aggree to accomplish its condition.


			Mini-Activity:
				Add another condition to our nested if statement:
					- check if username is atleast 8 characters long
					- check if the passwword is atelast 8 characters long.
					- show an alert which says "Thank you for logging in!"
				Add an else statement which will run if both condition were not met:
					- show an alert which says "Credentials too short".

			Stretch Goals:
				Add an else if statement that if username is less than 8 characters.
					- Show an alert "Username is too short"
				Add an eelse if statement if the password is less than 8 characters
					- show an alert "Password is too short"
		*/


// Function with Return Keyword

// let message = 'No message'; 
// console.log(message);

function determinTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return 'Not a typehoon yet.';
	}

	else if(windSpeed <= 61){
		return 'Tropical depression detected.';
	}

	else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.';
	}

	else if(windSpeed >= 89 && windSpeed <=117){
		return 'Severe tropical storm detected.'
	} 
	else {
		return 'Typhoon detected.';
	}
};

let message = determinTyphoonIntensity(68);
console.log(message);

	// console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if (message == 'Tropical storm detected.') {
		console.warn(message);
	}
	


// Truthy and Falsy
	// truthy
	if(true){
		console.log('Truthy');
	};

	if(1){
		console.log('True');
	};

	if([]) {
		console.log('Truthy')
	}



	// falsy
		// -0, "", null, NaN
	if(false){
		console.log('Falsy');
	}

	if(0) {
		console.log('False')
	}

	if(undefined){
		console.log('Falsy');
	}


// Ternary Operator
/*
	Syntax:
		(expression/condition) ? true : false;
	Three operands of ternary operator:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is false
*/

// let ternaryResult = (1 < 18) ? true : false;
// console.log(`Result of ternary operator ${ternaryResult}`);

	// let price = 50000;
	// price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000.");

	// let villain = "Harvey Dent"; 

	// villain === "Harvey Dent"
	// ? console.log("You lived long enough to be a villain.")
	// : console.log("Not quite villanous yet.")
	// // Ternary operators have an implicit "return" statement that without return keyword, the resulting expression can be stored in a variable.


	// // ELSE IF ternary operator
	// let a = 7;

	// a === 5
	// ? console.log("A")
	// : (a === 10? console.log("A is 10") : console.log("A is not 5 or 10"));


	// // Multiple statement execution

	// let name;

	// function isOfLegalAge() {
	// 	name = 'John';
	// 	return 'You are of the legal age limit';
	// }

	// function isUnderAge() {
	// 	name = 'Jane';
	// 	return 'You are under the age limit';
	// }

	// let age  = parseInt(prompt("What is you age?"));
	// console.log(age);

	// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	// console.log(`Result of Ternary Operator in functions: ${legalAge}, ${name}`);

	/*
	


	*/

	// let mon = 'Black';
	// let tues = 'Green';
	// let wed = 'Yellow';
	// let thurs = 'Red';
	// let fri = 'Violet';
	// let sat = 'Blue';
	// let sun = 'White';



	// function shirtColor(day){
	// 	if(day === 'Monday'){
	// 		return `Today is Monday, Wear ${mon}`;
	// 	}

	// 	else if(day === 'Tuesday'){
	// 		return `Today is Tuesday, Wear ${tues}`;
	// 	}
		
	// 	else if(day === 'Wednesday'){
	// 		return `Today is Wednesday, Wear ${wed}`;
	// 	}
		
	// 	else if(day === 'Thursday'){
	// 		return `Today is Thursday, Wear ${thurs}`;
	// 	}
		
	// 	else if(day === 'Friday'){
	// 		return `Today is Friday, Wear ${fri}`;
	// 	}
		
	// 	else if(day === 'Saturday'){
	// 		return `Today is Saturday, Wear ${sat}`;
	// 	}
		
	// 	else if(day === 'Friday'){
	// 		return `Today is Friday, Wear ${sun}`;
	// 	}
	// 	else {
	// 		return `Invalid Input. Please input a valid day of the week.`		
	// 	}
	// };

	// let today = shirtColor(prompt('Enter a day'));
	// alert(today);



			function colorOfTheDay(day) {
				//check if the argument passed is a string
				if(typeof day === "string"){

					if(day.toLowerCase() === "monday"){
						alert("Today is " + day + "." + "Wear Black")
					}
					else if(day.toLowerCase() === "tuesday"){
						alert(`Today is ${day}. Wear Green.`)
					}
					else if(day.toLowerCase() === "wednesday"){
						alert(`Today is ${day}. Wear Yellow.`)
					}
					else if(day.toLowerCase() === "thursday"){
						alert(`Today is ${day}. Wear Red.`)
					}
					else if(day.toLowerCase() === "friday"){
						alert(`Today is ${day}. Wear Violet.`)
					}
					else if(day.toLowerCase() === "saturday"){
						alert(`Today is ${day}. Wear Blue.`)
					}
					else if(day.toLowerCase() === "sunday"){
						alert(`Today is ${day}. Wear White.`)
					} 
					else {
						alert("Invalid input")
					}

				} 
				else {
					alert("Invalid Input. Enter a valid day of the week.")
				}
			}



// Switch Statement

	/*
		Syntax:
			switch (expression/condition) {
				case value:
					statement;
					break;
				default:
					statement;
					break;
			}
	*/

	// let hero = "Jose Rizal";

	// let hero = prompt("Enter a name: ").toLowerCase();

	// switch (hero) {
	// 	case "Jose Rizal":
	// 		console.log("National Hero of the Philippines");
	// 		break;
	// 	case "George Washington":
	// 		console.log("Hero of the American Revolution");
	// 		break;
	// 	case "hercules":
	// 		console.log("Legendary Hero of the Greek");
	// 		break;
	// 	default:
	// 		console.log("Please type again.");
	// 		break;
	// }

	// function roleChecker(role) {
	// 	switch(role){
	// 		case "admin":
	// 			console.log('Welcome Admin, to the dashboard');
	// 			break;
	// 		case "user":
	// 			console.log("You are not authorized to view this page.");
	// 			break;
	// 		case "guest":
	// 			console.log("Go to the registration page to register.");
	// 			break;
	// 		default:
	// 			console.log("Invalid Role");
	// 			break;
	// 			// by default, your switch ends with default case, so therefore, even if there is no break keyword in your default case, it will not run anything else.
	// 	}
	// }

	// let result = roleChecker(prompt("Who are you? ").toLowerCase());




// Try-Catch-Finally Statement
	// This is used for error handling


	function showIntensityAlert(windSpeed) {
		// Attemp to execute a code
		try {
			alerat(determinTyphoonIntensity(windSpeed));
		}
		catch (error){
			// error/err are commonly used variable by developers for storing errors.
			console.log(typeof error);
			console.warn(error.message) // Catch errors within the 'try' statement
		}
		finally {
			// continue execution of code REGARDLESS of success or failure of code execution in the 'try' block to handle/resolve errors.
			// optional 
			alert('Intensity updates will show new alert')
		}
	};

	showIntensityAlert(68);


// Throw - user-defined exception
	// Execution of the current function will stop

const number = 40;

try {
	if(number > 50){
		console.log("Success");
	}
	else{
		//user-defined throw statement
		throw Error("The number is low")
	}

	// If throw executes, the below code does not execute
	console.log("Hello");
}
catch(error){
	console.log("An error caught");
	console.warn(error.message);
}
finally {
	console.log("Please add a higher number");
}


	// Another exapmple

	function getArea(width, height) {
		if(isNaN(width) || isNaN(height)) {
			throw 'Parameter is not a number!'
		}
	}

	try {
		getArea(3,"A");
	}
	catch(e) {
		console.error(e)
	}
	finally {
		alert("Number Only");
	}